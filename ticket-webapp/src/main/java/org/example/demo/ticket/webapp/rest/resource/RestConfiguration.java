package org.example.demo.ticket.webapp.rest.resource;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.example.demo.ticket.webapp.rest.resource.projet.ProjetResource;
import org.example.demo.ticket.webapp.rest.resource.ticket.TicketResource;

@ApplicationPath("/*")
public class RestConfiguration extends Application
{
	private Logger log = Logger.getAnonymousLogger();

	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> classes = new HashSet<>();
		
		classes.add(ProjetResource.class);
		classes.add(TicketResource.class);
				
		return classes;
	}
	
}
