package org.example.demo.ticket.batch.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

import org.example.demo.ticket.batch.contract.TicketStatutProducter;
import org.example.demo.ticket.model.bean.ticket.TicketStatut;

import lombok.Setter;

public class TicketStatutProducterImpl implements TicketStatutProducter {

	
	private String pathToTicketStatus;

	@Override
	public void setPathToTicketStatus(String pathToTicketStatus) {
		this.pathToTicketStatus = pathToTicketStatus;
	}

	@Override
	public void createTicketStatus(List<TicketStatut> tickets) {
		StringBuilder sortie = new StringBuilder();
		
		tickets.forEach(ticket -> sortie.append(ticket.toString()).append("\n"));
		
		Path pathToFile = Paths.get(pathToTicketStatus+File.separator+"ticketsStatus.txt");
		
		try {
			Files.write(pathToFile, sortie.toString().getBytes(),StandardOpenOption.CREATE_NEW);			
			
		} catch (IOException e) {
			
			throw new RuntimeException("Impossible d'écrire le fichier " + pathToFile.toString() + "\n\r" + e.getLocalizedMessage() );
		}
		
		
		
	}
	
}
