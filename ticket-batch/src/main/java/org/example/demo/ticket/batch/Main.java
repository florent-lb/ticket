package org.example.demo.ticket.batch;


import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.example.demo.ticket.batch.contract.TicketStatutProducter;
import org.example.demo.ticket.business.interfaces.ManagerFactory;
import org.example.demo.ticket.model.bean.ticket.TicketStatut;
import org.example.demo.ticket.model.exception.TechnicalException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 * Classe Principale de lancement des Batches.
 *
 * @author lgu
 */
public class Main {

    /** Logger pour la classe */
    private static final Log LOGGER = LogFactory.getLog(Main.class);


    /**
     * The entry point of application.
     *
     * @param pArgs the input arguments
     * @throws TechnicalException sur erreur technique
     */
    public static void main(String[] pArgs) throws TechnicalException {
    	
    	ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:/bootstrapContext.xml");
    	
    	ManagerFactory factory = applicationContext.getBean("managerFactory",ManagerFactory.class);
    	
        try {
            if (pArgs.length < 1) {
                throw new TechnicalException("Veuillez préciser le traitement à effectuer !");
            }

            String vTraitementId = pArgs[0];
            if ("ExportTicketStatus".equals(vTraitementId)) 
            {
            	
            	TicketStatutProducter producter = applicationContext.getBean("statutProducter",TicketStatutProducter.class);            	
                LOGGER.info("Execution du traitement : ExportTicketStatus");
                
                List<TicketStatut> status =  factory.getTicketManager().getListTicketsStatus();
                producter.createTicketStatus(status);
                
                
                
            } else {
                throw new TechnicalException("Traitement inconnu : " + vTraitementId);
            }
        } catch (Throwable vThrowable) {
            LOGGER.error(vThrowable);
            System.exit(1);
        }
       
    }
}
