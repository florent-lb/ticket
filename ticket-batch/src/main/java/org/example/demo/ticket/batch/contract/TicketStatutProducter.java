package org.example.demo.ticket.batch.contract;

import java.util.List;

import org.example.demo.ticket.model.bean.ticket.TicketStatut;

public interface TicketStatutProducter {

	void setPathToTicketStatus(String pathToTicketStatus);

	void createTicketStatus(List<TicketStatut> tickets);
	
}
