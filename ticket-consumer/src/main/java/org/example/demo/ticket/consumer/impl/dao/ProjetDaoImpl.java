package org.example.demo.ticket.consumer.impl.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Optional;

import org.example.demo.ticket.consumer.contract.dao.ProjetDao;
import org.example.demo.ticket.model.bean.projet.Projet;
import org.example.demo.ticket.model.bean.projet.Version;
import org.example.demo.ticket.model.bean.utilisateur.Utilisateur;
import org.example.demo.ticket.model.exception.NotFoundException;
import org.example.demo.ticket.model.exception.TechnicalException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import lombok.extern.log4j.Log4j2;

public class ProjetDaoImpl extends AbstractDaoImpl implements ProjetDao {

	
	

	private RowMapper<Projet> projetRowMapper = new RowMapper<Projet>() {
		
		@Override
		public Projet mapRow(ResultSet rs, int rowNum) throws SQLException {
			Projet projet = new Projet(rs.getInt("id"));
			projet.setNom(rs.getString("nom"));
			projet.setDateCreation(rs.getDate("date_creation"));
			projet.setCloture(rs.getBoolean("cloture"));				
			projet.setResponsable(getUser(rs.getInt("responsable_id")));
			return projet;
		}
		
	};
	
	
	@Override
	public Projet getProjet(Integer idProjet) throws NotFoundException {
		
		
		String SQL = "SELECT * from projet WHERE id = :id_projet";
		
		MapSqlParameterSource params= new MapSqlParameterSource("id_projet", idProjet);
		
		NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(getDataSource());
		try
		{
			return template.queryForObject(SQL,params,projetRowMapper);
		}
		catch(EmptyResultDataAccessException ex)
		{
			throw new NotFoundException("Aucun projet trouvé pour l'ID : " + idProjet);
		}
	}

	@Override
	public List<Projet> getListProjet() {
		
		String SQL_LIST_PROJET = "SELECT * FROM projet ";		
		
		
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		
		
		
		
		return template.query(SQL_LIST_PROJET,projetRowMapper);
	}
	
	private Utilisateur getUser(Integer id)
	{
		String SQL_USER_BY_ID = "SELECT * FROM utilisateur WHERE id = :id_user";
		
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id_user", id,Types.INTEGER);
		
		NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(getDataSource());
		
		RowMapper<Utilisateur> utilisateurMapRow = new RowMapper<Utilisateur>() {
			
			@Override
			public Utilisateur mapRow(ResultSet rs, int rowNum) throws SQLException {
				Utilisateur user = new Utilisateur();
				user.setId(rs.getInt("id"));
				user.setNom(rs.getString("nom"));
				user.setPrenom(rs.getString("prenom"));
				return user;
			}
		};
		
		return template.queryForObject(SQL_USER_BY_ID, params,utilisateurMapRow);
		
	}

	@Override
	public void insertVersion(Version version) {
		
		String SQL = "insert into version VALUES (:id_projet,:numero)";
		
		
		TransactionTemplate txt = new TransactionTemplate(getPtxmanager());	
		txt.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		txt.setTimeout(30);
		txt.setName("tx_INSERT_VERSION");
		
		txt.execute(new TransactionCallbackWithoutResult() {
			
			@Override
			protected void doInTransactionWithoutResult(TransactionStatus status) {
				
				try
				{
				Projet projet = getProjet(version.getProjet().getId());
				Optional.ofNullable(version.getNumero())
						.filter(num -> num.length() > 0 && num.length() <= 30)
						.orElseThrow(() -> new TechnicalException("Le numéro " + version.getNumero() + " n'est pas un numéro valide"));
				
				MapSqlParameterSource params = new MapSqlParameterSource();
				params.addValue("id_projet", projet.getId(),Types.INTEGER);
				params.addValue("numero", version.getNumero(),Types.VARCHAR);
				NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(getDataSource());
				template.update(SQL, params);
				}
				catch(TechnicalException | NotFoundException ex)
				{
					status.setRollbackOnly();						
				}
				
			}
		});
		
		
		
		
		
		
		
		
		
		
	}

}
