package org.example.demo.ticket.consumer.contract.dao;

import java.util.List;

import org.example.demo.ticket.model.bean.projet.Projet;
import org.example.demo.ticket.model.bean.projet.Version;
import org.example.demo.ticket.model.exception.NotFoundException;

public interface ProjetDao {

	
	Projet getProjet(Integer idProjet) throws NotFoundException;
	
	List<Projet> getListProjet();

	void insertVersion(Version version);
	
}
