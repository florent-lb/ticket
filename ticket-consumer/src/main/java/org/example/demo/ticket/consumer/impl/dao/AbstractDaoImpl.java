package org.example.demo.ticket.consumer.impl.dao;

import javax.inject.Inject;
import javax.inject.Named;
import javax.sql.DataSource;

import org.springframework.transaction.PlatformTransactionManager;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.java.Log;
import lombok.extern.log4j.Log4j2;

public abstract class AbstractDaoImpl {

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Getter
	private DataSource dataSource;
	
	
	@Inject
	@Named("txManagerProjet")
	@Getter(value = AccessLevel.PROTECTED)
	private PlatformTransactionManager ptxmanager;
	
	
}
