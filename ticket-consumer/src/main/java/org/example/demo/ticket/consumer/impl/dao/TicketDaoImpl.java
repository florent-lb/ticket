package org.example.demo.ticket.consumer.impl.dao;

import java.util.List;

import javax.inject.Named;

import org.example.demo.ticket.consumer.contract.dao.TicketDao;
import org.example.demo.ticket.model.bean.ticket.TicketStatut;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

@Named
public class TicketDaoImpl extends AbstractDaoImpl implements TicketDao {

	@Override
	public List<TicketStatut> getListStatuts() {
		
		String SQL = "SELECT * from statut";
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		
		
		RowMapper<TicketStatut> rowMapTicketStatut = (resultSet, rowNum) ->
			
		{
				TicketStatut ts = new TicketStatut();
				ts.setId(resultSet.getInt("id"));
				ts.setLibelle(resultSet.getString("libelle"));
				return ts;
			
		};
		
		return template.query(SQL,rowMapTicketStatut);
	}
	
}
