package org.example.demo.ticket.business.impl;


import java.util.ArrayList;
import java.util.List;

import org.example.demo.ticket.business.interfaces.TicketManager;
import org.example.demo.ticket.consumer.contract.dao.TicketDao;
import org.example.demo.ticket.model.bean.projet.Projet;
import org.example.demo.ticket.model.bean.ticket.Bug;
import org.example.demo.ticket.model.bean.ticket.Evolution;
import org.example.demo.ticket.model.bean.ticket.Ticket;
import org.example.demo.ticket.model.bean.ticket.TicketStatut;
import org.example.demo.ticket.model.exception.NotFoundException;
import org.example.demo.ticket.model.recherche.ticket.RechercheTicket;

import lombok.Setter;


/**
 * Manager des beans du package Ticket.
 *
 * @author lgu
 */
public class TicketManagerImpl implements TicketManager {

	@Setter
	private TicketDao dao;
		
	
	@Override
	public List<TicketStatut> getListTicketsStatus()
	{
		return dao.getListStatuts();
	}
	
   
}
