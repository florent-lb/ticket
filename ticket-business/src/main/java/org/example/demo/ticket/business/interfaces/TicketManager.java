package org.example.demo.ticket.business.interfaces;

import org.example.demo.ticket.model.bean.ticket.TicketStatut;

import java.util.List;

public interface TicketManager {

	/**
	 * Permet de generer un rapport avec tous les status des tickets
	 */
	List<TicketStatut> getListTicketsStatus();
    
}
