package org.example.demo.ticket.business.impl;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.inject.Inject;

import org.apache.commons.lang3.ObjectUtils.Null;
import org.example.demo.ticket.business.interfaces.ProjetManager;
import org.example.demo.ticket.consumer.contract.dao.ProjetDao;
import org.example.demo.ticket.model.bean.projet.Projet;
import org.example.demo.ticket.model.bean.projet.Version;
import org.example.demo.ticket.model.exception.NotFoundException;


/**
 * Manager des beans du package Projet.
 *
 * @author lgu
 */
public class ProjetManagerImpl implements ProjetManager {

	
	private ProjetDao dao;
	
    @Override
    public Projet getProjet(Integer pId) throws NotFoundException { 
       
    	Optional.ofNullable(pId).orElseThrow(()-> new NullPointerException("L'ID ne doit pas être null"));
        return dao.getProjet(pId);
    }


    public void setDao(ProjetDao dao) {
		this.dao = dao;
	}


	@Override
    public List<Projet> getListProjet() {
        
    	return dao.getListProjet();
    	
    }


	@Override
	public void insertVersion(Version version) {
		Objects.requireNonNull(version, "La version ne peut pas être null");
		dao.insertVersion(version);
	}
}
